package cz.muni.fi.pb162.hw03.support;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Java doesn't support copy and delete of non-empty directories.
 * This interface is designed to fill this gap.
 * Also this project works with nio.Paths and for instance Apache Commons-IO works with io.Files.
 * Through this interface it can
 *
 * @author Josef Ludvicek
 */
public interface RecursiveExecutor {
    /**
     * Java alternative to linux {@code rm -rf path}.
     *
     * @param p path to delete (folder or file)
     * @throws IOException if delete fails
     */
    void deleteRecursively(Path p) throws IOException;

    /**
     * Java alternative to linux {@code cp -r from to}.
     *
     * @param from source dir/file
     * @param to target dir/file
     * @throws IOException
     */
    void copyRecursively(Path from, Path to) throws IOException;
}
