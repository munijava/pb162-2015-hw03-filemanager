package cz.muni.fi.pb162.hw03.support;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Implement copy and delete operations by calling linux {@code cp} and {@code rm} programs.
 *
 * @author Josef Ludvicek
 */
public class UnixExecutor implements RecursiveExecutor {

    /**
     * Command to execute in shell.
     * For instance {@code rm -rf *}.
     *
     * @param args command with arguments
     * @throws IOException if execution fails or {@code exitCode != 0}
     */
    static void executeCommand(String... args) throws IOException {
        ProcessBuilder pb = new ProcessBuilder(args);
        Process process = pb.start();
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            throw new IOException("Waiting for process to finish failed.", e);
        }
        if (process.exitValue() != 0) {
            throw new IOException("Failed to execute command: " + pb.command());
        }
    }

    @Override
    public void deleteRecursively(Path p) throws IOException {
        executeCommand("rm", "-rf", p.toAbsolutePath().toString());
    }

    @Override
    public void copyRecursively(Path from, Path to) throws IOException {
        executeCommand("cp", "-r", from.toAbsolutePath().toString(), to.toAbsolutePath().toString());
    }
}
